<?php

namespace Modules\App\Http\Middleware;

use App\Exceptions\ApiException;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\ParentUser;

class AccountTypeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$account_type)
    {
        $user = Auth::user();

        if (($account_type == 'parent' && $user instanceof ChildUser) || ($account_type == 'child' && $user instanceof ParentUser)) {
            throw ApiException::withMessage(['account_type' => "Sorry you don't have access to this area"],"Access denied");
        }

        return $next($request);
    }
}
