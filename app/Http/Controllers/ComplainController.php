<?php

namespace App\Http\Controllers;

use App\Models\Complain;
use Auth;
use Exception;
use Illuminate\Http\Request;

class ComplainController extends Controller
{
    public function index(){
        $complains=Complain::where('is_replied',0)->get();
        return view('admin.complains.index',compact('complains'));
    }
    public function view($id){
        $complain=Complain::where('id',$id)->first();
        return view('admin.complains.view',compact('complain'));
    }
    public function reply($id,Request $request){
        $data=$request->validate([
            'reply'=>'required|string',
        ]);
        $complain=Complain::where('id',$id)->first();
        $complain->reply=$data['reply'];
        $complain->is_replied=1;
        $complain->replied_by=Auth::user()->id;
        $complain->save();
        return redirect()->route('complain.index');
    }
    public function create(){
        
        return view('admin.complains.create');
    }
    public function store(Request $request){
        $data=$request->validate([
            'subject'=>'required|string',
            'description'=>'required|string',
        ]);
        $data['user_id']=Auth::user()->id;
        Complain::create($data);

        return redirect()->route('complain.index');
    }

    public function apiComplainCreate(Request $request){
        try{

            $data=$request->validate([
                'subject'=>'required|string',
                'description'=>'required|string',
            ]);
            $data['user_id']=Auth::user()->id;
            Complain::create($data);
            return response()->json(['msg'=>'success'],200);
        }catch(Exception $e){
            return response()->json(['msg'=>$e->getMessage()],500);
        }
    }
}
