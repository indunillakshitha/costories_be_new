<?php

namespace Modules\Core\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Core\Entities\FamilyEvent;
use Modules\Core\Events\FamilyEventDeleted;

class RecurringDeleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param FamilyEventCreated $event
     * @return void
     */
    public function handle(FamilyEventDeleted $eventCreated)
    {
        $event = $eventCreated->event;
        if($event->events()->exists())
            $events = $event->events()->pluck('id');
        else if($event->event)
            $events = $event->event->events()->whereDate('start_time', '>', $event->start_time)->pluck('id');
        else
            $events = [];

        FamilyEvent::whereIn('id', $events)->forceDelete();
    }
}
