<?php

namespace Modules\App\Entities;

use App\Models\Donation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory;

    protected $table ='transactions';
    protected $fillable = [
        'type',
        'owner_id',
        'parent_id',
        'amount',
        'donation_id',
        'status',
    ];

    /**
     * Get the user associated with the Transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function donationDetails()
    {
        return $this->hasOne(Donation::class, 'donation_id');
    }
    

   
}
