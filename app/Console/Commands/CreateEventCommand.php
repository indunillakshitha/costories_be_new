<?php

namespace App\Console\Commands;

use Artisan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Core\Entities\Family;
use Modules\Core\Entities\FamilyEvent;

class CreateEventCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $from = Carbon::now();

        for ($i=0; $i < 4; $i++) {
            $to = (clone $from)->addHour(1);
            $event = FamilyEvent::create([
                'family_id' => 1,
                'parent_id' => 1,
                'title' => 'Test event',
                'start_time' => $from,
                'end_time' => $to,
                'recurrence' => 'none',
            ]);
            $event->childUsers()->sync([1,2]);
            $from = $to;
        }

        return 0;
    }
}
