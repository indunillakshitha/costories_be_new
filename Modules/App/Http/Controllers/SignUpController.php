<?php

namespace Modules\App\Http\Controllers;

use App\Helpers\AccountHelper;
use DB;
use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Modules\App\Http\Requests\SignUpRequest;
use Modules\App\Notifications\SignUpNotification;
use Modules\Core\Entities\Family;
use Modules\Core\Entities\Invitation;
use Modules\Core\Entities\ParentUser;
use Illuminate\Http\Request;

/**
 * SignUpController
 */
class SignUpController extends Controller
{
    /**
     * Create new parent account and assign to a family
     *
     * @param  mixed $request
     * @return void
     */
    public function store(SignUpRequest $request)
    {

        $parent = DB::transaction(function () use ($request) {
            if ($request->has('invitationId')) {
                $invitation = Invitation::whereInvitedEmail($request['email'])->findOrFail($request['invitationId'], ['id', 'family_id']);
                $family = $invitation->family;
            } else {
                $family = Family::create([]);
            }

            /** @var ParentUser $parent */
            $parent = ParentUser::create(collect($request->validated())->put('birth_date', Carbon::createFromFormat('m / d / Y', $request['birthDate']))
                ->put('family_id', $family->id)->toArray());

            if (!$request->has('invitationId')) {
                $family->update(['primary_parent_id' => $parent->id]);
            }

            if ($request->hasFile('image')) {
                $parent->addMediaFromRequest('image')->toMediaCollection('profile_picture');
            }

            // $parent->notify(new SignUpNotification());

            return $parent;
        });

        $response = AccountHelper::projectAccount($parent);
        $response['token'] = $parent->createToken('app_token')->plainTextToken;

        if (!$request->has('invitationId')) {
            $response['redirect'] = config('incentiwise-path.payment_method');
        }

        return response()->json(['success' => $response]);
    }
    public function edit(Request $request)
    {


        try {


            $parent = ParentUser::where('id', $request->id)->first();


            $parent->name = $request->name;
            $parent->email = $request->email;
            $parent->birth_date = $request->birth_date;
            $parent->contact = $request->contact;
            $parent->relationship = $request->relationship;


            if ($request->hasFile('image')) {
                $parent->addMediaFromRequest('image')->toMediaCollection('profile_picture');
            }


            $parent->save();



            return response()->json(['success' => $parent],200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],500);
        }
    }
}
