<?php

namespace App\Helpers;

use App\Exceptions\ApiException;
use Auth;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\ParentUser;

class AccountHelper
{

    public static function findAccount($email)
    {
        $user = ParentUser::whereEmail($email)->first();
        if ($user) return $user;
        $user = ChildUser::whereEmail($email)->first();
        if ($user) return $user;
        return null;
    }

    /**
     * getParent
     *
     * @return ParentUser|ChildUser
     */
    public static function getAny()
    {
        $user = Auth::user();
        return $user;
    }

    /**
     * getParent
     *
     * @return ParentUser
     */
    public static function getParent()
    {
        $user = Auth::user();
        if ($user instanceof ChildUser) {
            throw ApiException::withMessage(['account_type' => "Sorry you don't have access to this area"],"Access denied");
        }
        return $user;
    }

    /**
     * getChild
     *
     * @return ChildUser
     */
    public static function getChild()
    {
        $user = Auth::user();
        if ($user instanceof ParentUser) {
            throw ApiException::withMessage(['account_type' => "Sorry you don't have access to this area"],"Access denied");
        }
        return $user;
    }

    public static function projectAccount($user)
    {
        $response = [
            'user' => $user,
        ];
        if ($user instanceof ParentUser) {
            $response['user_type'] = 'parent';
            $response['redirect'] = config('incentiwise-path.parent_home');
        } else if ($user instanceof ChildUser) {
            $response['user_type'] = 'child';
            $response['redirect'] = config('incentiwise-path.child_home');
        }
        return $response;
    }
}
