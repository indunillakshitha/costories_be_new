@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Donations</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Index</li>
                </ol>
                <a href="{{route('medals.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create
                    New</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{-- <h4 class="card-title">Data Export</h4> --}}
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <tr>
                                        <th>ID</th>
                                        {{-- <th>Family ID</th> --}}
                                        <th>Bronze</th>
                                        <th>Silver</th>
                                        <th>Gold</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <tr>
                                        <th>ID</th>
                                        {{-- <th>Family ID</th> --}}
                                        <th>Bronze</th>
                                        <th>Silver</th>
                                        <th>Gold</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </tr>
                            </tfoot>
                            <tbody>
                                
                                @isset($medals)
                                    @foreach ($medals as $medal)
                                        <tr>
                                            <td style="text-align:center">{{$medal->id}}</td>
                                            <td style="text-align:center">{{$medal->family_id}}</td>
                                            <td style="text-align:center">{{$medal->bronze}}</td>
                                            <td style="text-align:center">{{$medal->silver}}</td>
                                            <td style="text-align:center">{{$medal->gold}}</td>
                                            <td style="text-align:center">{{$medal->created_at}}</td>
                                                            <td style="text-align:center">
                                                <div class="btn btn-block btn-outline-success">

                                                    {{-- @can('medals.view')
                                                    <button type="button" onclick="window.location.href='/medals/view/{{$medal->id}}'" class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-info">View</button>
                                                    @endcan --}}
                                                    @can('medals.edit')
                                                    <button type="button" onclick="window.location.href='/medals/edit/{{$medal->id}}'" class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-info">Edit</button>
                                                    @endcan
                         
                                                  
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
