<?php

namespace Modules\App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\ParentUser;

class Shedule extends Model
{
    use HasFactory;

    protected $table ='shedules';
    protected $fillable = [
        'parent_id',
'child_id',
'event_id',
'description',
'from',
'to',
'date',
'family_id',
    ];
    
    protected static function newFactory()
    {
        // return \Modules\App\Database\factories\SheduleFactory::new();
    }

    /**
     * Get the user associated with the Shedule
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function child()
    {
        return $this->belongsTo(ChildUser::class, 'child_id');
    }
    public function parent()
    {
        return $this->hasOne(ParentUser::class, 'parent_id');
    }
}
