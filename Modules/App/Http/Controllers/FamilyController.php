<?php

namespace Modules\App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Helpers\AccountHelper;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * FamilyController
 */
class FamilyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->middleware('account_type:parent');
    }

    /**
     * Get family details of current user
     *
     * @return void
     */
    public function index(Request $request)
    {

        if ($request->has('children')) {
            $children = AccountHelper::getParent()
            ->family()
            ->first(['id'])
            ->children()
            ->get(['id','name','email','family_id']);

            return response()->json(['success' => $children]);
        }


        $family = AccountHelper::getParent()
            ->family()
            ->with(['children:id,name,email,family_id','parents:id,name,email,family_id,relationship'])
            ->first(['id']);

        return response()->json(['success' => $family]);
    }
}
