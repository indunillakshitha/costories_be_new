<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Modules\Core\Entities\ParentUser
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $contact
 * @property string $birth_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $family_id
 * @property-read \Modules\Core\Entities\Family $family
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\Invitation[] $invitations
 * @property-read int|null $invitations_count
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereFamilyId($value)
 * @property string $relationship
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereRelationship($value)
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read mixed $profile_picture
 * @property string|null $fcm_token
 * @method static \Illuminate\Database\Eloquent\Builder|ParentUser whereFcmToken($value)
 */
class ParentUser extends Model implements HasMedia
{
    use InteractsWithMedia;
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    const Relationship =  ['Father', 'Mother', 'Uncle', 'Aunty', 'Grand Father', 'Grand Mother', 'Guardian'];

    protected $fillable = ['name', 'email', 'contact', 'birth_date', 'relationship', 'family_id','fcm_token'];

    protected $hidden = ['media'];

    protected $appends = ['profile_picture'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(200)
              ->height(200)
              ->sharpen(10);
    }

    public function getProfilePictureAttribute()
    {
        if ($this->hasMedia('profile_picture')){
            try {
                return $this->getFirstMedia('profile_picture')->getTemporaryUrl(Carbon::now()->addMinutes(30),'thumb');
            } catch (\Throwable $th) {
                return $this->getFirstMedia('profile_picture')->getUrl('thumb');
            }
        }
        return 'https://picsum.photos/200';
    }

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }
}
