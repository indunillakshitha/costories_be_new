<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\ParentUser;
use Faker\Factory as Faker;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\Family;
use Modules\Core\Entities\Invitation;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $faker = Faker::create();

        $family = Family::create([]);

        $parent = ParentUser::create([
            'name' => $faker->firstName,
            'email' => $faker->email,
            'contact' => $faker->phoneNumber,
            'relationship' => 'mother',
            'birth_date' => $faker->dateTime,
            'family_id' => $family->id
        ]);

        $family->update([
            'primary_parent_id' => $parent->id
        ]);

        for ($i=0; $i < 5; $i++) {
            ChildUser::create([
                'name' => $faker->firstName,
                'email' => $faker->email,
                'birth_date' => $faker->dateTime,
                'family_id' => $family->id
            ]);
        }

        Invitation::create([
            'family_id' => $family->id,
            'parent_id' => $parent->id,
            'invited_email' => 'nadunnew@gmail.com',
            'invitation_type' => 'parent'
        ]);

        Invitation::create([
            'family_id' => $family->id,
            'parent_id' => $parent->id,
            'invited_email' => 'incentiwise2021@gmail.com',
            'invitation_type' => 'parent'
        ]);
    }
}
