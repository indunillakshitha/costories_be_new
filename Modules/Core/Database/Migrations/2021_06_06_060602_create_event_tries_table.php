<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_tries', function (Blueprint $table) {
            $table->id();

            $table->foreignId('event_id')->references('id')->on('family_events');

            $table->foreignId('child_id')->references('id')->on('child_users');
            $table->text('child_comment');
            $table->foreignId('parent_id')->nullable()->references('id')->on('parent_users');
            $table->text('parent_comment')->nullable();
            
            $table->boolean('response')->nullable();
            $table->dateTime('response_at')->nullable();
            $table->tinyInteger('status')->default(0)->comment('1 means complete the try');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_tries');
    }
}
