<?php

return [
    'login' => '/login',
    'account_type' => '/account-types',
    'child_invitation' => '/children-invitations',
    'parent_invitation' => '/parent-invitations',
    'parent_home' => '/parent-home',
    'child_home' => '/child-home',
    'parent_signup' => '/parent-signUp',
    'payment_method' => '/payment-method',
];
