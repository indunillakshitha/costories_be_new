<?php

use App\Http\Controllers\ComplainController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\MedalController;
use App\Http\Controllers\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\App\Http\Controllers\ChildEventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('complain/store', [ComplainController::class,'apiComplainCreate']);
 //medals
 Route::get('medals/all/{familyId}',[MedalController::class,'all']);

 //transactions
 Route::get('wallet-balance/{childId}',[WalletController::class,'getChildWalletBalance']); //send child_id in path parameters


 // send  "type","child_id","amount"   
//  type =1 for to savngs account =2 for donate
//  if the type = 2 send "donation_id"
 Route::post('wallet-request-transaction',[WalletController::class,'requestForTransaction']); 


 Route::get('wallet-requests-pending/{family_id}/{status}',[WalletController::class,'pendingTransactionRequests']);//send family_id in path parameters

 //send transaction_id and status in path parameters
//  type =1 for  processed 2=for rejected
 Route::get('wallet-process-transaction/{transaction_id}/{status}',[WalletController::class,'pendingTransactionRequestsProcess']);

 Route::get('payment/individualPayment/{id}',[WalletController::class,'indivitualPayements']);

//  Route::get('medals/all/{familyId}','MedalController@all');