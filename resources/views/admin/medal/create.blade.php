@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Medals</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">index</li>
                </ol>
                <a href="{{ route('donation.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create Medal </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="swalDefaultSuccess">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <form role="form" action="{{ route('medals.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                      
                        <div class="form-group">
                            <label for="contact">parent</label>
                            <input type="text" class="form-control" id="family_id" name="family_id" placeholder="Enter Parent id">
                        </div>
                        <div class="form-group">
                            <label for="contact">Bronze</label>
                            <input type="text" class="form-control" id="bronze" name="bronze" placeholder="Enter Bronze value">
                        </div>
                        <div class="form-group">
                            <label for="email">Silver</label>
                            <input type="text" class="form-control" id="silver" name="silver" placeholder="Enter Silver value">
                        </div>
                        <div class="form-group">
                            <label for="account_type">Gold</label>
                            <input type="text" class="form-control" id="gold" name="gold" placeholder="Enter Gold Value ">
                        </div>
                   
               


                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {{-- <button type="button" class="btn btn-primary" onclick="alerts()">Submit</button> --}}
                </form>
            </div>
            <!-- /.card -->
        </div>

    </div><!-- /.container-fluid -->

    </div>
    <!-- /.row -->



@endsection
