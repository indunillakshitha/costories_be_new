<?php

namespace Modules\App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Helpers\AccountHelper;
use Auth;
use Carbon\Carbon;
use DB;
use Event;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\App\Http\Requests\CurrentAppVersionRequest;
use Modules\App\Http\Requests\FamilyEventUpdateRequest;
use Modules\Core\Entities\Family;
use Modules\Core\Entities\FamilyEvent;

/**
 * FamilyEventController
 */
class FamilyEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->middleware('account_type:parent')->only(['store','update']);
    }

    public function index(Request $request)
    {
        $from = Carbon::now()->startOfDay();
        $to = Carbon::now()->endOfDay();

        if ($request->has('from')) {
            $from = Carbon::parse($request['from']);
            $to = (clone $from)->endOfDay();
        }

        if ($request->has('to')) {
            $to = Carbon::parse($request['to']);
        }

        /** @var FamilyEvent */
        $events = AccountHelper::getAny()
            ->family()
            ->first()
            ->events()
            ->where(function ($query) use ($from, $to) {
                $query->where(function ($query) use ($from) {
                    $query->where('start_time', '<=', $from)
                        ->where('end_time', '>=', $from);
                })->orWhere(function ($query) use ($to) {
                    $query->where('start_time', '<=', $to)
                        ->where('end_time', '>=', $to);
                })->orWhere(function ($query) use ($from, $to) {
                    $query->where('start_time', '>=', $from)
                        ->where('end_time', '<=', $to);
                });
            })->with('childUsers:id,name')
            ->withCount('tries')
            ->orderBy('start_time')
            ->get([
                'id',
                'title',
                'start_time',
                'end_time',
            ])->map->getFormattedTimes();

        return response()->json(['success' => $events]);
    }

    public function indexDates(Request $request)
    {
        /** @var FamilyEvent */
        $eventDates = AccountHelper::getAny()
            ->family()
            ->first()
            ->events()
            ->select(DB::raw('DATE(start_time) AS dates'), DB::raw('COUNT(id) AS count'))
            ->groupBy('dates')
            ->pluck('count', 'dates');

        return response()->json(['success' => $eventDates]);
    }

    public function show($id)
    {
        /** @var FamilyEvent */
        $event = AccountHelper::getAny()
            ->family()
            ->first()
            ->events()
            ->with([
                'childUsers:id,name',
                'parentUser:id,name,relationship',
                'tries:id,event_id,child_comment,parent_comment,response,status'
            ])->find($id, [
                'id',
                'parent_id',
                'title',
                'instruction',
                'start_time',
                'end_time',
                'recurrence',
            ]);

        $event = collect($event)
        ->put('event_files', $event->getMedia('event_files'))
        ->put('event_data', $event->fetchFormattedTimes());

        return response()->json(['success' => $event]);
    }

    public function store(FamilyEventUpdateRequest $request)
    {
       
        DB::transaction(function () use ($request) {
            /** @var FamilyEvent */
            $event = AccountHelper::getParent()
            ->family()
            ->first()
            ->events()
            ->create(
                collect($request->validated())
                ->put('parent_id', AccountHelper::getParent()->id)
                ->toArray()
            );

            $event->childUsers()->sync($request['child_users']);
        });

        return response()->json(['success' => true]);
    }


    public function update(FamilyEventUpdateRequest $request,$id)
    {
        DB::transaction(function () use ($request,$id) {
            /** @var FamilyEvent */
            $event = AccountHelper::getParent()
            ->family()
            ->first()
            ->events()
            ->find($id);

            $event->childUsers()->sync($request['child_users']);

            $event->update($request->validated());
        });

        return response()->json(['success' => true]);
    }


    public function delete($id){
        try{

            $event = FamilyEvent::where('id',$id)->first();

            if(isset($event)){
         
                $event->delete();
                return response()->json(['success' => 'Event Deleted'],200);
            }

            return response()->json(['success' => 'Event Not Found'],200);
            
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()],500);
        }
    }
}
