@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Complains</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">View</li>
                </ol>
                <a href="{{ route('complain.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">View Complain </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="swalDefaultSuccess">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif



                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text" class="form-control" id="" name="" value="{{$complain->subject}}" placeholder="Enter subject">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="" id="" value=""  class="form-control"cols="30" rows="10">{{$complain->description}}</textarea>
                        </div>


                <form role="form" action="{{ route('complain.reply', $complain->id) }}" method="POST">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Reply</label>
                            <textarea name="reply" id="reply"  class="form-control"cols="30" rows="10"></textarea>
                        </div>


                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Reply</button>
                        </div>
                        {{-- <button type="button" class="btn btn-primary" onclick="alerts()">Submit</button> --}}
                </form>
            </div>
            <!-- /.card -->
        </div>

    </div><!-- /.container-fluid -->

    </div>
    <!-- /.row -->



@endsection
