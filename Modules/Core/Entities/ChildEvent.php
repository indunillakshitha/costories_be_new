<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Modules\Core\Entities\ChildEvent
 *
 * @property int $id
 * @property int $child_user_id
 * @property int $family_event_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Entities\ChildUser $childUser
 * @property-read \Modules\Core\Entities\FamilyEvent $familyEvent
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent whereChildUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent whereFamilyEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildEvent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ChildEvent extends Model
{    use InteractsWithMedia;
    protected $fillable = ['child_user_id','family_event_id','status'];
    
    protected $hidden = ['media'];
   
    protected $appends = ['event_picture'];

    public function childUser() {
        return $this->belongsTo(ChildUser::class,'child_user_id','id');
    }

    public function familyEvent() {
        return $this->belongsTo(FamilyEvent::class,'family_event_id','id');
    }


   public function getEventPictureAttribute()
    {
        if ($this->hasMedia('event_picture')){
            try {
                return $this->getFirstMedia('event_picture')->getTemporaryUrl(Carbon::now()->addMinutes(30),'thumb');
            } catch (\Throwable $th) {
                return $this->getFirstMedia('event_picture')->getUrl('thumb');
            }
        }
        return 'https://picsum.photos/200';
    }
}
