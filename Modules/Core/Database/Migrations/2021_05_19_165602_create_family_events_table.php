<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('family_id')->references('id')->on('families');
            $table->foreignId('parent_id')->references('id')->on('parent_users');
            $table->string('title')->nullable();
            $table->string('instruction')->nullable();
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->string('recurrence');
            $table->foreignId('family_event_id')->nullable()->references('id')->on('family_events');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_events');
    }
}
