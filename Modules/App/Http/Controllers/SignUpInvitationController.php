<?php

namespace Modules\App\Http\Controllers;

use App\Helpers\AccountHelper;
use Exception;
use Illuminate\Routing\Controller;
use Modules\App\Http\Requests\InvitationAcceptRejectRequest;
use Modules\App\Http\Requests\InvitationRequest;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\Invitation;
use Modules\Core\Entities\ParentUser;

use Socialite;
use Validator;
use Illuminate\Http\Request;
/**
 * SignUpInvitationController
 */
class SignUpInvitationController extends Controller
{
    /**
     * Get received invitations
     *
     * @param  mixed $request
     * @return void
     */
    public function index(InvitationRequest $request)
    {
        $user = Socialite::driver($request['type'])->stateless()->userFromToken($request['token']);

        return response()->json(['success' => Invitation::signupInvitations($request['user_type'], $user->email)]);
    }

    /**
     * Accept selected invitations and create child account
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function acceptInvitations(InvitationAcceptRejectRequest $request, $id)
    {
        $user = Socialite::driver($request['type'])->stateless()->userFromToken($request['token']);

        $invitation = Invitation::whereInvitedEmail($user->email)->findOrFail($id, ['id', 'invitation_type', 'family_id']);

        if ($invitation['invitation_type'] == Invitation::ChildType) {
            $databaseUser = ChildUser::create([
                'name' => $user->name,
                'email' => $user->email,
                'family_id' => $invitation->family_id,
                'birth_date' => $invitation->birth_date
            ]);

            $response = AccountHelper::projectAccount($databaseUser);
            $response['token'] = $databaseUser->createToken('app_token')->plainTextToken;

            return response()->json(['success' => $response]);
        }
    }

    /**
     * Accept selected invitations
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function rejectInvitations(InvitationAcceptRejectRequest $request, $id)
    {
        $user = Socialite::driver($request['type'])->stateless()->userFromToken($request['token']);

        $invitation = Invitation::whereInvitedEmail($user->email)->findOrFail($id, ['id']);

        return response()->json(['success' => $invitation->delete()]);
    }


    public function newInvitations(Request $request){
        
        try{


            $validator = Validator::make($request->all(),[
                'parent_email' => 'required|email',
                'invited_email' => 'required|email',
            ]);

            if($validator->fails()){
                return response()->json(['error' => $validator->messages()],400);

            } else{

                $parent = ParentUser::where('email',$request->parent_email)->first();


            $result= isset($parent) ? $parent : [];
            if(isset($parent)){
                $invitation =new Invitation();
                $invitation->family_id=$parent->family_id;
                $invitation->parent_id =$parent->id;
                $invitation->invited_email=$request->invited_email;
                $invitation->invitation_type=$request->type;
                $invitation->save();
                // if($request->type=='child'){

                //     $childUser =new ChildUser();
                //     $childUser->name=$request->name;
                //     $childUser->email=$request->invited_email;
                //     $childUser->birth_date=$request->birth_date;
                //     $childUser->family_id=$parent->family_id;
                //     $childUser->save();
                // }
            }


            $msg= isset($parent) ? 'succeedded' : 'Parent Not Fount';

            return response()->json(['success' => $msg],200);

            }
        }catch(Exception $e){

            return response()->json(['error' => $e->getMessage()],500);
            
        }
    }
}
