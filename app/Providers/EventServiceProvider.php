<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Core\Events\FamilyEventCreated;
use Modules\Core\Events\FamilyEventDeleted;
use Modules\Core\Events\FamilyEventUpdated;
use Modules\Core\Listeners\RecurringCreated;
use Modules\Core\Listeners\RecurringDeleted;
use Modules\Core\Listeners\RecurringUpdated;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\Google\\GoogleExtendSocialite@handle',
        ],

        // FamilyEvent events
        FamilyEventCreated::class => [
            RecurringCreated::class
        ],
        FamilyEventUpdated::class => [
            RecurringUpdated::class
        ],
        FamilyEventDeleted::class => [
            RecurringDeleted::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
