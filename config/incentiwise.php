<?php

return [
    'android_version' => env('ANDROID_VERSION','1.0.0'),
    'ios_version' => env('IOS_VERSION','1.0'),
];
