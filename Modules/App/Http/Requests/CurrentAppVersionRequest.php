<?php

namespace Modules\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrentAppVersionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'os' => 'required|in:android,ios',
            'version' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
