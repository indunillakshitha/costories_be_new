@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Complains</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Index</li>
                </ol>
                <a href="{{route('complain.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create
                    New</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{-- <h4 class="card-title">Data Export</h4> --}}
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Subject</th>
                                        <th>Desctiption</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Subject</th>
                                        <th>Desctiption</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </tr>
                            </tfoot>
                            <tbody>
                                
                                @isset($complains)
                                    @foreach ($complains as $complain)
                                        <tr>
                                            <td style="text-align:center">{{$complain->id}}</td>
                                            <td style="text-align:center">{{$complain->user_id}}</td>
                                            <td style="text-align:center">{{$complain->title}}</td>
                                            <td style="text-align:center">{{$complain->description}}</td>
                                            <td style="text-align:center">{{$complain->created_at}}</td>
                                                            <td style="text-align:center">
                                                <div class="btn btn-block btn-outline-success">

                                                    @can('complain.view')
                                                    <button type="button" onclick="window.location.href='/complains/view/{{$complain->id}}'" class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-info">Reply</button>
                                                    @endcan
                         
                                                  
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
