@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Complains</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">View</li>
                </ol>
                <a href="{{ route('complain.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">View Donation </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="swalDefaultSuccess">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif



                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="" name="" value="{{ $donation->name }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Contact</label>
                        <input type="text" class="form-control" id="" name="" value="{{ $donation->contact }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="text" class="form-control" id="" name="" value="{{ $donation->email }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Account Type</label>
                        <input type="text" class="form-control" id="" name="" value="{{ $donation->account_type }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Account Number</label>
                        <input type="text" class="form-control" id="" name="" value="{{ $donation->account_number }}">
                    </div>



                </div>
                <!-- /.card -->
            </div>

        </div><!-- /.container-fluid -->

    </div>
    <!-- /.row -->



@endsection
