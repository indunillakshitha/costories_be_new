<?php

namespace Modules\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FamilyEventUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'instruction' => 'nullable|max:500',
            'recurrence' => 'in:none,daily,weekly,monthly',
            'start_time' => 'required',
            'end_time' => 'required',
            'child_users' => 'required|array',
            'child_users.*' => 'exists:child_users,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
