<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Sanctum\HasApiTokens;


/**
 * Modules\Core\Entities\ChildUser
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $birth_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $family_id
 * @property-read \Modules\Core\Entities\Family $family
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereFamilyId($value)
 * @property string|null $fcm_token
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereFcmToken($value)
 */
class ChildUser extends Model
{
    use HasApiTokens;
    use HasFactory;

    protected $fillable = ['name','email','birth_date','family_id','fcm_token'];

    protected $hidden = ['pivot'];

    public function family()
    {
        return $this->belongsTo(Family::class);
    }
}
