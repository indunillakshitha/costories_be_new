<?php

namespace Modules\App\Http\Controllers;

use App\Models\Medal;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\App\Entities\Shedule;
use Modules\Core\Entities\ParentUser;
use Validator;

class SheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('app::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('app::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try{


            $validator = Validator::make($request->all(),[
                
                'parent_id' => 'required|integer',
                'from' => 'required',
                'to' => 'required',
                'date' => 'required',
                'description' => 'required|string',
            ]);

            if($validator->fails()){
                return response()->json(['error' => $validator->messages()],400);

            } else{

                $family_id=ParentUser::where('id',$request->parent_id)->first()->family_id;
                
                foreach($request->child_id as $child_user){
                    
                    $shedule=new Shedule();
                    $shedule->parent_id=$request->parent_id;
                    $shedule->family_id=$family_id;
                    $shedule->from=$request->from;
                    $shedule->to=$request->to;
                    $shedule->date=$request->date;
                    $shedule->description=$request->description;
                    $shedule->child_id=$child_user;
                    $shedule->save();
                }

          
            $msg= isset($shedule) ? 'succeedded' : 'Something Wrong';

            return response()->json(['success' => $msg],200);

            }
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()],500);
        }
    
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('app::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('app::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function today($childId){
        try{

            $shedeles=Shedule::where('child_id',$childId)->whereDate('date',Carbon::today())->get();   
          
            $msg= isset($shedeles) ? 'succeedded' : 'Something Wrong';

            return response()->json(['success' => $shedeles],200);

            
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()],500);
        }
    }

    public function all($familyId){
        try{
        
            $medal= Medal::where('family_id',$familyId)->first();
            if(!$medal){
                $medal= Medal::where('family_id',0)->first();
            }
            return response()->json(['success' => ['data'=>$medal]],200);
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }
    public function allShedulesForParent($familyId){
        try{

            $shedeles=Shedule::where('family_id',$familyId)->get();   
          
            $msg= isset($shedeles) ? 'succeedded' : 'Something Wrong';

            return response()->json(['success' => $shedeles],200);

            
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()],500);
        }
    }
    public function getSheduleDate($sheduleId){
        try{

            $shedule=Shedule::with('child')->where('id',$sheduleId)->first();   
            // $date=$shedule->date;
            
            return response()->json(['success' => $shedule],200);

            
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()],500);
        }
    }
}
