@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Users</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">edit</li>
                </ol>
                <a href="{{ route('users.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit User </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ route('users.update', $users->id) }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" value="{{ $users->name }}" name="name"
                                placeholder="Enter Name">
                        </div>
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" id="email" value="{{ $users->email }}" name="email"
                                placeholder="Enter Email">
                        </div>
                        <div class="form-group">
                            <label for="name">Mobile</label>
                            <input type="text" class="form-control" id="mobile" value="{{ $users->mobile }}" name="mobile"
                                placeholder="Enter Mobile No">
                        </div>
                        <div class="form-group">
                            <label for="name">Roles</label>

                            @foreach ($roles as $role)

                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="" name="roles[]"
                                        value="{{ $role->id }}" @foreach ($users->roles as $my_perm)  @if ($role->id==$my_perm->id)
                                    checked @endif
                            @endforeach>
                            <label class="form-check-label " for="exampleCheck1">{{ $role->name }}</label>
                        </div>
                        @endforeach
                    </div>
                    @can('users.direct.Permissions')
                        <div class="form-group">
                            <label for="name">Direct Permissions</label>
                            <div class="row">
                                @foreach ($all_permissions as $permission)
                                    <div class="form-check">
                                        <div class="col-3">
                                            <div class="row">
                                                <div class="col-3">
                                                    <input type="checkbox" class="form-check-input" id="" name="permissions[]"
                                                        value="{{ $permission->id }}" @foreach ($my_permissions->permissions as $my_perm)  @if ($permission->id==$my_perm->id)
                                                    checked @endif
                                @endforeach>
                            </div>
                            <div class="col-9">
                                <label class="form-check-label" for="exampleCheck1">{{ $permission->display_name }}</label>
                            </div>
                        </div>
                </div>
            </div>
            @endforeach
        </div>
        </div>
    @endcan
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
    </div>
    <!-- /.card -->
    </div>

    </div><!-- /.container-fluid -->




@endsection
