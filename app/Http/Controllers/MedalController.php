<?php

namespace App\Http\Controllers;

use App\Models\Medal;
use Exception;
use Illuminate\Http\Request;

class MedalController extends Controller
{
    public function index(){
        $medals=Medal::all();
        return view('admin.medal.index',compact('medals'));
    }
    public function view($id){
        $medal=Medal::where('id',$id)->first();
        return view('admin.medal.view',compact('medal'));
    }
    public function edit($id){
        $medal=Medal::where('id',$id)->first();
        return view('admin.medal.edit',compact('medal'));
    }
    public function create(){
        
        return view('admin.medal.create');
    }
    public function store(Request $request){
        $data=$request->validate([
            'family_id'=>'required|numeric',
            'bronze'=>'required|numeric',
            'silver'=>'required|numeric',
            'gold'=>'required|numeric',
        ]);
        Medal::create($request->all());
        
        return redirect()->route('medals.index');
    }
    public function update(Request $request,$id){
        $data=$request->validate([
            'family_id'=>'required|numeric',
            'bronze'=>'required|numeric',
            'silver'=>'required|numeric',
            'gold'=>'required|numeric',
        ]);
        $medal=Medal::where('id',$id)->first();
        $medal->bronze=$request->bronze;
        $medal->silver=$request->silver;
        $medal->gold=$request->gold;
    
        $medal->save();

        return redirect()->route('medals.index');
    }


    public function all($familyId){
        try{
        
            $medal= Medal::where('family_id',$familyId)->first();
            if(!$medal){
                $medal= Medal::where('family_id',0)->first();
            }
            return response()->json(['success' => ['data'=>$medal]],200);
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }
    
}
