<?php

namespace Modules\App\Http\Controllers;

use App\Exceptions\ApiException;
use Illuminate\Routing\Controller;
use Modules\App\Http\Requests\CurrentAppVersionRequest;

/**
 * AppController
 */
class AppController extends Controller
{
    /**
     * Check app version matching with server
     *
     * @param  mixed $request
     * @return void
     */
    public function getCurrentAppVersion(CurrentAppVersionRequest $request)
    {
        if (config('incentiwise.'.$request['os'].'_version') != $request['version']){
            throw ApiException::withMessage(['update'=>'You have to update to continue'],'There is new update on this app');
        }
        return response()->json(['success' => 'App is upto date']);
    }
}
