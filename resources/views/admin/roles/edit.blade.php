@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Roles</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Roles</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
                <a href="{{ route('roles.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Role </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ route('roles.update', $my_permissions->id) }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Role Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ $my_permissions->name }}" placeholder="Enter Role Name">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li class="swalDefaultSuccess">{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.card-body -->
                    @foreach ($all_permissions as $permission)

                        <div class="form-check ml-3">
                            <input type="checkbox" class="form-check-input" id="permission" name="permission[]"
                                value="{{ $permission->id }}" @foreach ($my_permissions->permissions as $my_perm) 
                                @if ($permission->id==$my_perm->id)
                            checked @endif
                    @endforeach >
                    <label class="form-check-label" for="exampleCheck1">{{ $permission->display_name }}</label>
            </div>
            @endforeach

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
        <!-- /.card -->
    </div>

    </div><!-- /.container-fluid -->

@endsection
