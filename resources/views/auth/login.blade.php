<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href={{asset("/UI/vendor/perfect-scrollbar.css")}} rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href={{asset("/UI/css/app.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/app.rtl.css")}} rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href={{asset("/UI/css/vendor-material-icons.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-material-icons.rtl.css")}} rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.rtl.css")}} rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.rtl.css")}} rel="stylesheet">





</head>

<body class="layout-login-centered-boxed">





    <div class="layout-login-centered-boxed__form">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-2 navbar-light">
            <a href="index.html" class="navbar-brand text-center mb-2 mr-0" style="min-width: 0">
                <!-- LOGO -->


                <span class="ml-2">ADMIN</span>
            </a>
        </div>

        <div class="card card-body">





            <form action="/login"  method="POST">
                @csrf
                <div class="form-group">
                    <label class="text-label" for="email_2">Email Address:</label>
                    <div class="input-group input-group-merge">
                        <input name="email" type="email"  class="form-control form-control-prepended" placeholder="Email your address"
                        required
                        autocomplete="email"
                        autofocus
                        >
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-label" for="password_2">Password:</label>
                    <div class="input-group input-group-merge">
                        <input name="password" type="password"  class="form-control form-control-prepended" placeholder="Enter your password"
                        required
                        autocomplete="current-password"
                        >

                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>

                @error('email')
                <div class="alert alert-danger d-flex" role="alert">
                        <i class="material-icons mr-3">error_outline</i>
                        <div class="text-body"><strong>Invalid E-mail or Password</strong></div>
                </div>
                @enderror

                <div class="form-group">
                    <button class="btn btn-block btn-primary" type="submit">Login</button>
                </div>
                <div class="form-group text-center">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" checked="" id="remember">
                        <label class="custom-control-label" for="remember">Remember me</label>
                    </div>
                </div>
                <div class="form-group text-center">
                    <a href="">Forgot password?</a> <br> <br>
                    Don't have an account? <br> <a class="text-body text-underline"

                    href="/register"
                    > Register here !</a>

                </div>
            </form>
        </div>
    </div>


    <!-- jQuery -->
    <script src={{asset("/UI/vendor/jquery.min.js")}}></script>

    <!-- Bootstrap -->
    <script src={{asset("/UI/vendor/popper.min.js")}}></script>
    <script src={{asset("/UI/vendor/bootstrap.min.js")}}></script>

    <!-- Perfect Scrollbar -->
    <script src={{asset("/UI/vendor/perfect-scrollbar.min.js")}}></script>

    <!-- DOM Factory -->
    <script src={{asset("/UI/vendor/dom-factory.js")}}></script>

    <!-- MDK -->
    <script src={{asset("/UI/vendor/material-design-kit.js")}}></script>

    <!-- Range Slider -->
    <script src={{asset("/UI/vendor/ion.rangeSlider.min.js")}}></script>
    <script src={{asset("/UI/js/ion-rangeslider.js")}}></script>

    <!-- App -->
    <script src={{asset("/UI/js/toggle-check-all.js")}}></script>
    <script src={{asset("/UI/js/check-selected-row.js")}}></script>
    <script src={{asset("/UI/js/dropdown.js")}}></script>
    <script src={{asset("/UI/js/sidebar-mini.js")}}></script>
    <script src={{asset("/UI/js/app.js")}}></script>



</body>

</html>
