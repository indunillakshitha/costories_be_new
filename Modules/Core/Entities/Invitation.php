<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Parent_;

/**
 * Modules\Core\Entities\Invitation
 *
 * @property int $id
 * @property int $family_id
 * @property int $parent_id
 * @property string $invited_email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereInvitedEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $invitation_type
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereInvitationType($value)
 * @property-read \Modules\Core\Entities\Family $family
 * @property-read \Modules\Core\Entities\ParentUser $invitedBy
 */
class Invitation extends Model
{
    //Invitation Types
    const ParentType = 'parent';
    const ChildType = 'child';

    protected $fillable = ['family_id', 'parent_id', 'invited_email', 'invitation_type'];

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function invitedBy()
    {
        return $this->belongsTo(ParentUser::class, 'parent_id', 'id');
    }

    public static function signupInvitations($userType,$email)
    {
        return Invitation::with([
            'family:id,primary_parent_id',
            'invitedBy:id,name,email',
            'family.children:id,name,email,family_id'
        ])->whereInvitationType($userType)->whereInvitedEmail($email)->get(['id', 'family_id', 'parent_id']);
    }
}
