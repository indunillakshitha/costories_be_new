<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Illuminate\Http\Request;
use Modules\App\Entities\Transaction;
use Modules\App\Entities\Wallet;
use Modules\Core\Entities\ChildUser;
use PhpParser\Builder\Trait_;

class WalletController extends Controller
{
    
    public function getChildWalletBalance($childId){
        try{
            $virtualBalance =Wallet::where('wallet_type',1)->where('child_id',$childId)->first();
            $balance =$virtualBalance->balance;
            return response()->json(['success'=>$balance]);

        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
    public function requestForTransaction(Request $request){
        try{
            DB::beginTransaction();
            $transacton =new Transaction();
            $transacton->type=$request->type;
            $transacton->owner_id=$request->child_id;
            $child=ChildUser::where('id',$request->child_id)->first();
            $transacton->family_id=$child->family_id;
            $transacton->amount=$request->amount;
            if($request->type==2){
                $transacton->donation_id=$request->donation_id;

            }
            $wallet =Wallet::where('child_id',$request->child_id)->where('wallet_type',1)->first();
            $wallet->balance -= $transacton->amount;
            $wallet->save();
            $transacton->save();
            DB::commit();
            return response()->json(['success'=>$transacton]);

        }catch(Exception $e){
            DB::rollback();
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
    public function pendingTransactionRequests($familyId,$status){
        try{
            $transactons=Transaction::where('family_id',$familyId)
                        ->where('type',$status)
                        ->where('status',0)->get();
            
            return response()->json(['success'=>$transactons]);

        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
    }
    public function pendingTransactionRequestsProcess($transactionId,$status){
        try{
            DB::beginTransaction();
            $transacton=Transaction::where('id',$transactionId)->first();
            $transacton->status=$status;
            if($status==2){
                $wallet =Wallet::where('child_id',$transacton->owner_id)->where('wallet_type',1)->first();
                $wallet->balance += $transacton->amount;
                $wallet->save();
            }
            $transacton->save();

            DB::commit();
            return response()->json(['success'=>'done']);

        }catch(Exception $e){
            DB::rollback();
            return response()->json(['error'=>$e->getMessage()]);
        }
    }

    public function indivitualPayements($id){
        $transacton=Transaction::where('id',$id)->first();
        return response()->json(['success'=>$transacton]);
    }
}
