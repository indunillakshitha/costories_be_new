<?php

use Illuminate\Http\Request;
use Modules\App\Http\Controllers\FamilyEventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/app')->group(function () {
    // App info routes
    Route::get('/version', 'AppController@getCurrentAppVersion');

    // Auth related routes
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::get('/profile', 'AuthController@getCurrentUser');

    // Signup routes
    Route::prefix('/sign-up')->group(function () {
        Route::post('', 'SignUpController@store');

        // Invitation routes
        Route::post('/invitation', 'SignUpInvitationController@index');
        Route::post('/invitation/{id}/accept', 'SignUpInvitationController@acceptInvitations');
        Route::post('invitation/{id}/reject', 'SignUpInvitationController@rejectInvitations');
    });

    // Family routes
    Route::get('/family', 'FamilyController@index');

    // Events routes
    Route::apiResource('/events', 'FamilyEventController')->only(['index','show','store','update']);
    Route::get('/events/delete/{id}', 'FamilyEventController@delete');
    Route::get('/event-dates', 'FamilyEventController@indexDates');

    //invitations new
    Route::post('/sendInvitation','SignUpInvitationController@newInvitations');
    //invitations new
    Route::post('/profile/edit','SignUpController@edit');

    Route::get('/child/events/{id}','ChildEventController@all');
    Route::get('/child/family-details/{id}','ChildEventController@details');
    
    
    //medals
    Route::get('/app/medals/all/{familyId}','SheduleController@all');
    
    //shedule
    Route::get('shedule/today/{childId}','SheduleController@today');
    Route::post('shedule/store','SheduleController@store');
    Route::get('shedule/all-for-parent/{parent_id}','SheduleController@allShedulesForParent');
    Route::get('shedule/individualTask/{sheduleId}','SheduleController@getSheduleDate');
    
    
    
    
    Route::post('event-submit','ChildEventController@submit');
    Route::get('event/pending-confirmation/{familyId}','ChildEventController@pendings');
    Route::post('event/confirm','ChildEventController@confirm');
    Route::get('/family/child/parents/{childId}','ChildEventController@childParents');
});

// event tries
Route::post('event/submit','ChildEventController@submit');
