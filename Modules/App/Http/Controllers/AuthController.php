<?php

namespace Modules\App\Http\Controllers;

use App\Helpers\AccountHelper;
use Auth;
use Illuminate\Routing\Controller;
use Modules\App\Http\Requests\InvitationAcceptRejectRequest;
use Modules\App\Http\Requests\InvitationDeleteRequest;
use Modules\App\Http\Requests\InvitationRejectRequest;
use Modules\App\Http\Requests\InvitationRequest;
use Modules\App\Http\Requests\TokenCallbackRequest;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\Invitation;
use Modules\Core\Entities\ParentUser;
use Illuminate\Http\Request;
use Socialite;

/**
 * AuthController
 */
class AuthController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:sanctum')->only(['getCurrentUser']);
    }

    /**
     * Use for login process of apps
     *
     * @param  mixed $request
     * @return void
     */
    public function login(TokenCallbackRequest $request)
    {
        $user = Socialite::driver($request['type'])->stateless()->userFromToken($request['token']);

        $databaseUser = AccountHelper::findAccount($user->email);

        if ($databaseUser == null) {
            return response()->json(['success' => [
                'redirect' => config('incentiwise-path.account_type'),
                'user_data' => [
                    'name' => $user->name,
                    'email' => $user->email,
                    'image' => $user->avatar
                ]
            ]]);
        }

        $response = AccountHelper::projectAccount($databaseUser);
        $response['token'] = $databaseUser->createToken('app_token')->plainTextToken;

        return response()->json(['success' => $response]);
    }

    /**
     * Get current logged user
     *
     * @return void
     */
    public function getCurrentUser()
    {
        return response()->json(['success' => AccountHelper::projectAccount(request()->user())]);
    }


    public function logout(Request $request){
        $user=Auth::user();
        $request->user()->currentAccessToken()->delete();
    }
}
