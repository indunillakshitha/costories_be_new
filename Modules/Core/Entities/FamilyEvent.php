<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Event;
use Modules\Core\Events\FamilyEventCreated;
use Modules\Core\Events\FamilyEventDeleted;
use Modules\Core\Events\FamilyEventUpdated;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


/**
 * Modules\Core\Entities\FamilyEvent
 *
 * @property int $id
 * @property int $family_id
 * @property int $parent_id
 * @property string $title
 * @property string $start_time
 * @property string $end_time
 * @property string $recurrence
 * @property int|null $family_event_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read FamilyEvent|null $event
 * @property-read \Illuminate\Database\Eloquent\Collection|FamilyEvent[] $events
 * @property-read int|null $events_count
 * @property-read \Modules\Core\Entities\Family $family
 * @property-read \Modules\Core\Entities\ParentUser $invitedBy
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent newQuery()
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereFamilyEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereRecurrence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent withoutTrashed()
 * @mixin Eloquent
 * @property string|null $instruction
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ChildUser[] $childUsers
 * @property-read int|null $child_users_count
 * @property-read \Modules\Core\Entities\ParentUser $parentUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\EventTry[] $tries
 * @property-read int|null $tries_count
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereInstruction($value)
 */
class FamilyEvent extends Model implements HasMedia
{
    use InteractsWithMedia;
    use SoftDeletes;

    protected $fillable = [
        'family_id',
        'parent_id',
        'title',
        'instruction',
        'start_time',
        'end_time',
        'recurrence',
        'family_event_id'
    ];

    protected $hidden = ['pivot'];

    public static function boot()
    {
        parent::boot();

        self::created(function (FamilyEvent $model) {
            FamilyEventCreated::dispatch($model);
        });

        self::updated(function (FamilyEvent $model) {
            FamilyEventUpdated::dispatch($model);
        });

        self::deleted(function (FamilyEvent $model) {
            FamilyEventDeleted::dispatch($model);
        });
    }

    public function tries()
    {
        return $this->hasMany(EventTry::class,'event_id');
    }

    public function events()
    {
        return $this->hasMany(FamilyEvent::class, 'family_event_id', 'id');
    }

    public function event()
    {
        return $this->belongsTo(FamilyEvent::class, 'family_event_id');
    }

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function parentUser()
    {
        return $this->belongsTo(ParentUser::class, 'parent_id', 'id');
    }

    public function childUsers()
    {
        return $this->belongsToMany(ChildUser::class, 'child_events');
    }

    public function fetchFormattedTimes()
    {
        $now = Carbon::now();

        $status = "invalid";

        if (Carbon::parse($this->start_time)->isAfter($now)){
            $status = "not_started";
        }else if (Carbon::parse($this->start_time)->isBefore($now) && Carbon::parse($this->end_time)->isAfter($now)){
            $status = "ongoing";
        }else if (Carbon::parse($this->end_time)->isBefore($now)){
            $status = "ended";
        }

        return collect()
            ->put('date', Carbon::parse($this->start_time)->format('d M Y'))
            ->put('start', Carbon::parse($this->start_time)->format('h:i A'))
            ->put('end', Carbon::parse($this->end_time)->format('h:i A'))
            ->put('event_status',$status);
    }

    public function getFormattedTimes()
    {
        return collect($this)->merge($this->fetchFormattedTimes());
    }
}
