<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * Modules\Core\Entities\Family
 *
 * @property int $id
 * @property int $primary_parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Family newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Family newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Family query()
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family wherePrimaryParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $gold_start_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ChildUser[] $children
 * @property-read int|null $children_count
 * @property-read mixed $family_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ParentUser[] $parents
 * @property-read int|null $parents_count
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereGoldStartAmount($value)
 * @property-read \Modules\Core\Entities\ParentUser|null $primaryParent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\FamilyEvent[] $events
 * @property-read int|null $events_count
 */
class Family extends Model
{
    use HasFactory;

    protected $fillable = ['primary_parent_id'];

    protected $appends = ['family_count'];

    public function getFamilyCountAttribute()
    {
        return $this->children()->count() + $this->parents()->count();
    }

    public function events()
    {
        return $this->hasMany(FamilyEvent::class);
    }

    public function children()
    {
        return $this->hasMany(ChildUser::class);
    }

    public function parents()
    {
        return $this->hasMany(ParentUser::class);
    }

    public function primaryParent()
    {
        return $this->belongsTo(ParentUser::class,'primary_parent_id');
    }
}
