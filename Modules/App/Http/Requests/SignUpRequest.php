<?php

namespace Modules\App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Entities\ParentUser;

class SignUpRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|email:rfc,dns',
            'contact' => 'required|numeric',
            'birthDate' => 'required|date_format:m / d / Y|before:'.Carbon::parse('-20 Years')->format('Y/d/m'),
            'relationship' => 'required|in:' . collect(ParentUser::Relationship)->implode(','),
            'image' => 'nullable|file|mimes:jpg,jpeg,png,bmp,tiff|max:4096',
            'invitationId' => 'nullable|exists:invitations,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
