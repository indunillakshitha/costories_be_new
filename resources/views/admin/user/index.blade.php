@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Users</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">index</li>
                </ol>
                <a href="{{route('users.create')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create
                    New</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{-- <h4 class="card-title">Data Export</h4> --}}
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align:center">ID</th>
                                    <th style="text-align:center">User</th>
                                    <th style="text-align:center">Email</th>
                                    <th style="text-align:center">Role</th>
                                    <th style="text-align:center">Status</th>
                                    <th style="text-align:center">Created</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th style="text-align:center">ID</th>
                                    <th style="text-align:center">User</th>
                                    <th style="text-align:center">Email</th>
                                    <th style="text-align:center">Role</th>
                                    <th style="text-align:center">Status</th>
                                    <th style="text-align:center">Created</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                
                                @isset($users)
                                    @foreach ($users as $user)
                                        <tr>
                                            <td style="text-align:center">{{ $user->id }}</td>
                                            <td style="text-align:center">{{ $user->name }}</td>
                                            <td style="text-align:center">{{ $user->email }}</td>
                                            <td style="text-align:center">
                                                @foreach ($user->roles as $role)

                                                    <span class=" badge bg-primary"> {{ $role->name }}</span>

                                                @endforeach
                                            </td>
                                            <td style="text-align:center">
                                                @if ($user->is_blocked == 1)
                                                    <span class=" badge bg-danger">BLOCKED</span>
                                                @else
                                                    <span class=" badge bg-primary">ACTIVE</span>
                                                @endif
                                            </td>
                                            <td style="text-align:center">{{ $user->created_at }}</td>
                                            <td style="text-align:center">
                                                <div class="btn btn-block btn-outline-success">
                                                    @can('users.block')

                                                        @if ($user->is_blocked == 1)
                                                            <button type="button" onclick="block({{ $user->id }},'users/block')"
                                                                class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-primary">UNBLOCK</button>
                                                        @else
                                                            <button type="button" onclick="block({{ $user->id }},'users/block')"
                                                                class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-danger">BLOCK</button>

                                                        @endif
                                                    @endcan
                                                    @can('users.edit')

                                                        <button type="button" class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-info"
                                                            onclick="window.location.href='/users/edit/{{ $user->id }}'">
                                                            EDIT</button>
                                                    @endcan
                                                    @can('users.changePassword')

                                                        <button type="button" class="ml-2 btn waves-effect waves-light btn-rounded btn-xs btn-info"
                                                            onclick="window.location.href='/users/edit/password/{{ $user->id }}'">
                                                            PASSWORD</button>
                                                    @endcan
                                                  
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endisset

                            </tbody>
                            <div class="ml-5 mt-2">
                                {{ $users->links() }}

                            </div>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
