<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 */
	class User extends \Eloquent {}
}

namespace Modules\Core\Entities{
/**
 * Modules\Core\Entities\ChildUser
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $birth_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $family_id
 * @property-read \Modules\Core\Entities\Family $family
 * @method static \Illuminate\Database\Eloquent\Builder|ChildUser whereFamilyId($value)
 */
	class ChildUser extends \Eloquent {}
}

namespace Modules\Core\Entities{
/**
 * Modules\Core\Entities\Family
 *
 * @property int $id
 * @property int $primary_parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Family newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Family newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Family query()
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family wherePrimaryParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $gold_start_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ChildUser[] $children
 * @property-read int|null $children_count
 * @property-read mixed $family_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Entities\ParentUser[] $parents
 * @property-read int|null $parents_count
 * @method static \Illuminate\Database\Eloquent\Builder|Family whereGoldStartAmount($value)
 * @property-read \Modules\Core\Entities\ParentUser|null $primaryParent
 */
	class Family extends \Eloquent {}
}

namespace Modules\Core\Entities{
/**
 * Modules\Core\Entities\FamilyEvent
 *
 * @property int $id
 * @property int $family_id
 * @property int $parent_id
 * @property string $title
 * @property string $start_time
 * @property string $end_time
 * @property string $recurrence
 * @property int|null $family_event_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read FamilyEvent|null $event
 * @property-read \Illuminate\Database\Eloquent\Collection|FamilyEvent[] $events
 * @property-read int|null $events_count
 * @property-read \Modules\Core\Entities\Family $family
 * @property-read \Modules\Core\Entities\ParentUser $invitedBy
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent newQuery()
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereFamilyEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereRecurrence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FamilyEvent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FamilyEvent withoutTrashed()
 * @mixin Eloquent
 */
	class FamilyEvent extends \Eloquent implements \Spatie\MediaLibrary\HasMedia {}
}

namespace Modules\Core\Entities{
/**
 * Modules\Core\Entities\Invitation
 *
 * @property int $id
 * @property int $family_id
 * @property int $parent_id
 * @property string $invited_email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereInvitedEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $invitation_type
 * @method static \Illuminate\Database\Eloquent\Builder|Invitation whereInvitationType($value)
 * @property-read \Modules\Core\Entities\Family $family
 * @property-read \Modules\Core\Entities\ParentUser $invitedBy
 */
	class Invitation extends \Eloquent {}
}
