<?php

namespace Modules\App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wallet extends Model
{
    use HasFactory;

    protected $table ='wallet';
    protected $fillable = [
        'child_id',
        'wallet_type',
        'balance',
    ];
    
   
}
