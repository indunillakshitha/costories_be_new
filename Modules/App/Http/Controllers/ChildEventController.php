<?php

namespace Modules\App\Http\Controllers;

use App\Models\Medal;
use Auth;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Intervention\Image\Commands\ResponseCommand;
use Modules\Core\Entities\ChildEvent;
use Modules\Core\Entities\ChildUser;
use Modules\Core\Entities\EventTry;
use Modules\Core\Entities\FamilyEvent;
use Validator;
use Illuminate\Support\Facades\DB;
use Modules\App\Entities\Transaction;
use Modules\App\Entities\Wallet;

class ChildEventController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('app::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('app::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('app::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('app::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function all($id){
        try{
        $events=ChildEvent::whereChildUserId($id)->where('status',0)->get();
        return response()->json(['data'=>$events],200);
    }catch(Exception $e){
        return response()->json(['error'=>$e->getMessage()],500);
    }
    }

    public function details($id){

        // $details=ChildUser::leftJoin('parent_users','parent_users.family_id','child_users.family_id')
        //                     ->where('child_users.id',$id)
        //                     ->select('child_users.*'
        //                     ,'parent_users.name as p_name',
        //                     'parent_users.email  as p_email ',
        //                     'parent_users.contact as p_contact',
        //                     'parent_users.birth_date as p_birth_date',
        //                     'parent_users.relationship as p_relationship',
        //                     )
        //                     ->get();
        try{
        
            $details= ChildUser::with(['family.parents'])->get();
            return response()->json(['data'=>$details],200);
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }

    public function submit(Request $request){
        
        try {
            $validator = Validator::make($request->all(),[ 

                'event_id' => 'required',
                'child_id' => 'required',
                'child_comment' => 'required|string',
             
            ]);
            if($validator->fails()){
                return response()->json(['error'=>$validator->messages()],401);
            }else{
                DB::beginTransaction();
                $data=$request->all();
                $data['child_id']=$request->child_id;
                $count=EventTry::where('id',$request->event_tries_id)->where('child_id',$data['child_id'])->count();
                $data['family_id']=FamilyEvent::where('id',$request->event_id)->first()->family_id;
                $medals=Medal::where('family_id',$data['family_id'])->first();
                if(!isset($medals)){

                    $medals=Medal::where('family_id',0)->first();

                }

                if(isset($count) && $count==4){
                    $data['status']=1;
                    $child_event=ChildEvent::where('family_event_id',$request->event_id)->where('child_user_id',$request->child_id)->first();
                    $child_event->status=1;
                    $child_event->save();
                    $wallet =Wallet::where('child_id',$data['child_id'])->where('wallet_type',2)->first();
                    if(isset($wallet)){
                      
                        $wallet->balance+=$medals->gold;
                        $wallet->save();
                    }else{
    
                        $wallet=[];
                        $wallet['child_id']=1;
                        $wallet['wallet_type']=2;
                        $wallet['balance']= $medals->gold;
                        Wallet::create($wallet);
                    }
                }
                if ($request->hasFile('event_picture')) {
                    $child_event->addMediaFromRequest('event_picture')->toMediaCollection('event_picture');
                }
           

                $eventTry =[];
                $eventTry['event_id']=$request->event_id;
                $eventTry['child_id']=$request->child_id;
                $eventTry['family_id']=$request->family_id;
                $eventTry['child_comment']=$request->child_comment;
             
                $event_try =EventTry::create($eventTry);

                DB::commit();
            }
            
            return response()->json(['success'=>$event_try],200);
            
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }
    public function pendings($familyId){
        try {

           $pendings=EventTry::with('getEventPictureAttribute')->where('status',0)->where('family_id',$familyId)->get();
            
            return response()->json(['success'=>$pendings],200);
            
        } catch (Exception $e) {
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }

    public function confirm(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 

                'event_tries_id' => 'required',
                'parent_id' => 'required',
                'parent_comment' => 'required',
                'status' => 'required',
             
            ]);

           $data=$request->all();
           
           if($validator->fails()){
               return response()->json(['error'=>$validator->messages()],401);
           }else{
               
           DB::beginTransaction();
           $event_try=EventTry::where('id',$request->event_tries_id)->first();
           $event_try->parent_id=$request->parent_id;
           $event_try->parent_comment=$request->parent_comment;
           $event_try->status=$request->status;
           
           $transaction=[];

            $medals=Medal::where('family_id',$event_try->family_id)->first();
            if(!isset($medals)){

                $medals=Medal::where('family_id',0)->first();

            }
            $count=EventTry::where('id',$request->event_tries_id)->where('child_id',$event_try->child_id)->count();
            $transaction['type']=1;
            $transaction['owner_id']=$event_try->child_id;
            $transaction['parent_id']=$event_try->parent_id;
            
            if($count==1){
                $transaction['amount']=$medals->gold;
            }
            else if($count==2){
                $transaction['amount']=$medals->silver;
            }
            else if($count==3){
                $transaction['amount']=$medals->bronze;
            }
 
            // Transaction::create([$transaction]);
            
            $wallet =Wallet::where('child_id',$event_try->child_id)->where('wallet_type',1)->first();
            if(isset($wallet)){
                $wallet->balance+=$medals->gold;
                $wallet->save();
            }else{
            $wallet=[];
            $wallet['child_id']=$event_try->child_id;
            $wallet['wallet_type']=1;
            $wallet['balance']= $transaction['amount'];
            Wallet::create($wallet);
            }

            
                $child_event=ChildEvent::where('family_event_id',$event_try->event_id)->where('child_user_id',$event_try->child_id)->first();
                $child_event->status=$request->status;
                $child_event->save();
                $event_try->save();
           
            DB::commit();

            return response()->json(['success'=>$event_try],200);
            
           }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['error'=>$e->getMessage()],500);
        }
    }

    public function childParents($childId){
        $data=ChildUser::with('family.parents')->where('id',$childId)->first();
        return response()->json(['success'=>$data],200);
    }
}
