<?php

/***************************************
 * File name: AdminController
 * Author: IcCoBoDe_deV
 * Email: skgindunil@gmail.com
 * Date: 6/12/2021
 * Time: 09:00 PM
 ***************************************/
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
   public function index(){
       return view('admin.index');
   }
}
