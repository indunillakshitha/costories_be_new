<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use Auth;
use Illuminate\Http\Request;

class DonationController extends Controller
{
    public function index(){
        $donations=Donation::all();
        return view('admin.donations.index',compact('donations'));
    }
    public function view($id){
        $donation=Donation::where('id',$id)->first();
        return view('admin.donations.view',compact('donation'));
    }
    public function edit($id){
        $donation=Donation::where('id',$id)->first();
        return view('admin.donations.edit',compact('donation'));
    }
    public function create(){
        
        return view('admin.donations.create');
    }
    public function store(Request $request){

        // return $request;
        $data=$request->validate([
            'name'=>'required|string',
            'account_number'=>'required|integer',
        ]);
        Donation::create($request->all());

        return redirect()->route('donation.index');
    }
    public function update(Request $request,$id){
        $data=$request->validate([
            'name'=>'required|string',
            'account_number'=>'required|integer',
        ]);
        $donation=Donation::where('id',$id)->first();
        $donation->name=$request->name;
        $donation->contact=$request->contact;
        $donation->email=$request->email;
        $donation->account_type=$request->account_type;
        $donation->account_number=$request->account_number;
        $donation->save();

        return redirect()->route('donation.index');
    }

    
}
