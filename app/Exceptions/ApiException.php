<?php

namespace App\Exceptions;

use Exception;

class ApiException extends Exception
{

    private $errors = [];

    private function __construct($title, $errors)
    {
        parent::__construct($title);
        $this->errors = $errors;
    }

    public static function withMessage($errors, $title = 'The given data was invalid.')
    {
        return new ApiException($title, $errors);
    }

    public function getResponse()
    {
        return response()->json(['message' => $this->getMessage(), 'errors' => collect($this->errors)->map(function($error){
            return [$error];
        })], 422);
    }

    public function render()
    {
        return response()->json(['message' => $this->getMessage(), 'errors' => collect($this->errors)->map(function($error){
            return [$error];
        })], 422);
    }
}
