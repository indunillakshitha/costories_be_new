<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ComplainController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\MedalController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/***************************************
 * File name: web.php
 * Author: IcCoBoDe_deV
 * Email: skgindunil@gmail.com
 * Date: 6/12/2021
 * Time: 09:00 PM
 ***************************************/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/home', [AdminController::class,'index'])->name('home');
    Route::get('/', [AdminController::class,'index'])->name('home');
    Route::get('/admin',[AdminController::class,'index'])->name('admin.index');



    Route::prefix('users')->group(function () {

        Route::get('/index',[UserController::class,'index'])->middleware(['can:users.index'])->name('users.index');
        Route::get('/create',[UserController::class,'create'])->middleware(['can:users.create'])->name('users.create');
        Route::post('/store',[UserController::class,'store'])->middleware(['can:users.store'])->name('users.store');
        Route::post('/users/block',[UserController::class,'block'])->middleware(['can:users.block'])->name('users.block');
        Route::get('/edit/{id}',[UserController::class,'edit'])->middleware(['can:users.edit'])->name('users.edit');
        Route::get('/edit/password/{id}',[UserController::class,'changePasswordView'])->middleware(['can:users.changePassword'])->name('users.changePasswordView');
        Route::post('/edit/password',[UserController::class,'changePassword'])->middleware(['can:users.changePassword'])->name('users.changePassword');
        Route::post('/update/{id}',[UserController::class,'update'])->middleware(['can:users.update'])->name('users.update');
        Route::post('/search}',[UserController::class,'search'])->middleware(['can:users.serch'])->name('users.serch');
    
    });
    Route::prefix('roles')->group(function () {
    
        Route::get('/index',[RoleController::class,'index'])->middleware(['can:roles.index'])->name('roles.index');
        Route::get('/create',[RoleController::class,'create'])->middleware(['can:roles.create'])->name('roles.create');
        Route::post('/store',[RoleController::class,'store'])->middleware(['can:roles.store'])->name('roles.store');
        Route::get('/edit/{id}',[RoleController::class,'edit'])->middleware(['can:roles.edit'])->name('roles.edit');
        Route::post('/update/{id}',[RoleController::class,'update'])->middleware(['can:roles.update'])->name('roles.update');
        Route::post('roles/delete',[RoleController::class,'delete'])->middleware(['can:roles.delete'])->name('roles.delete');
    
    });
    Route::prefix('complains')->group(function () {
    
        Route::get('/index',[ComplainController::class,'index'])->middleware(['can:complain.index'])->name('complain.index');
        Route::get('/view/{id}',[ComplainController::class,'view'])->middleware(['can:complain.view'])->name('complain.view');
        Route::post('/reply/{id}',[ComplainController::class,'reply'])->middleware(['can:complain.reply'])->name('complain.reply');
        Route::get('/create',[ComplainController::class,'create'])->middleware(['can:complain.create'])->name('complain.create');
        Route::post('/create',[ComplainController::class,'store'])->middleware(['can:complain.create'])->name('complain.store');

    
    });
    Route::prefix('donations')->group(function () {
    
        Route::get('/index',[DonationController::class,'index'])->middleware(['can:donations.index'])->name('donation.index');
        Route::get('/view/{id}',[DonationController::class,'view'])->middleware(['can:donations.view'])->name('donation.view');
        Route::get('/edit/{id}',[DonationController::class,'edit'])->middleware(['can:donations.edit'])->name('donation.edit');
        Route::get('/create',[DonationController::class,'create'])->middleware(['can:donations.create'])->name('donation.create');
        Route::post('/store',[DonationController::class,'store'])->middleware(['can:donations.store'])->name('donation.store');
        Route::post('/update/{id}',[DonationController::class,'update'])->middleware(['can:donations.update'])->name('donation.update');

    
    });
    Route::prefix('medals')->group(function () {
    
        Route::get('/index',[MedalController::class,'index'])->middleware(['can:medals.index'])->name('medals.index');
        Route::get('/view/{id}',[MedalController::class,'view'])->middleware(['can:medals.view'])->name('medals.view');
        Route::get('/edit/{id}',[MedalController::class,'edit'])->middleware(['can:medals.edit'])->name('medals.edit');
        Route::get('/create',[MedalController::class,'create'])->middleware(['can:medals.create'])->name('medals.create');
        Route::post('/store',[MedalController::class,'store'])->middleware(['can:medals.store'])->name('medals.store');
        Route::post('/update/{id}',[MedalController::class,'update'])->middleware(['can:medals.update'])->name('medals.update');

    
    });
});