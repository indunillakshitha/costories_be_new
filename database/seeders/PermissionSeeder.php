<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array =[


            //Users Section
            ['section_name'=>'users','name' => 'users.index'],
            ['name' => 'users.create','section_name'=>'users'],
            ['name' => 'users.store','section_name'=>'users'],
            ['name' => 'users.block','section_name'=>'users'],
            ['name' => 'users.update','section_name'=>'users'],
            ['name' => 'users.edit','section_name'=>'users'],
            ['name' => 'users.changePassword','section_name'=>'users'],

            //Roles Section
            ['name' => 'roles.index','section_name'=>'role'],
            ['name' => 'roles.create','section_name'=>'role'],
            ['name' => 'roles.store','section_name'=>'role'],
            ['name' => 'roles.delete','section_name'=>'role'],
            ['name' => 'roles.update','section_name'=>'role'],
            ['name' => 'roles.edit','section_name'=>'role'],
            //complain Section
            ['name' => 'complain.index','section_name'=>'complain'],
            ['name' => 'complain.view','section_name'=>'complain'],
            ['name' => 'complain.create','section_name'=>'complain'],
            ['name' => 'complain.store','section_name'=>'complain'],
            ['name' => 'complain.reply','section_name'=>'complain'],
            //donations Section
            ['name' => 'donations.index','section_name'=>'donations'],
            ['name' => 'donations.view','section_name'=>'donations'],
            ['name' => 'donations.edit','section_name'=>'donations'],
            ['name' => 'donations.store','section_name'=>'donations'],
            ['name' => 'donations.create','section_name'=>'donations'],
            ['name' => 'donations.update','section_name'=>'donations'],
            //medal Section
            ['name' => 'medals.index','section_name'=>'medals'],
            ['name' => 'medals.view','section_name'=>'medals'],
            ['name' => 'medals.edit','section_name'=>'medals'],
            ['name' => 'medals.store','section_name'=>'medals'],
            ['name' => 'medals.create','section_name'=>'medals'],
            ['name' => 'medals.update','section_name'=>'medals'],
 
        ];



        //************ UNCOMMENT THESE SECTION ON FIRST SEED************//
        $user_n ['name']='Super admin';
        $user_n ['email']='suadmin@gmail.com';
        $user_n ['password']=Hash::make('Abcd@1234');

        $role=Role::where('name','Super Admin')->first();
        if(!$role){
            $role = Role::create(['name' => 'Super Admin']);

        }
        foreach($permission_array as $permission){

            $check_has_permission = Permission::where('name', $permission['name'])->first();
            if(!isset($check_has_permission)){
                Permission::create($permission);
            }

            $role->givePermissionTo($permission['name']);
        }
        
        $user =User::where('email','suadmin@gmail.com')->first();
        if(!$user){

            $user = User::create($user_n);
        }
        $user->assignRole('Super Admin');
    }
}
