<?php

namespace Modules\Core\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Entities\FamilyEvent;

class FamilyEventUpdated
{
    use SerializesModels,Dispatchable;

    public FamilyEvent $event;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(FamilyEvent $event)
    {
        $this->event = $event;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
