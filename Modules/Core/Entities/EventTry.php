<?php

namespace Modules\Core\Entities;

use Carbon\Carbon;
use Event;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Modules\Core\Entities\EventTry
 *
 * @property int $id
 * @property int $event_id
 * @property int $child_id
 * @property string $child_comment
 * @property int|null $parent_id
 * @property string $parent_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereChildComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereChildId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereParentComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $response
 * @property string $response_at
 * @property-read \Modules\Core\Entities\Family $family
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventTry whereResponseAt($value)
 */
class EventTry extends Model implements HasMedia
{
    use InteractsWithMedia;
    protected $fillable = ['event_id','child_id','child_comment','parent_id','parent_comment','response','status','response_at','family_id','status'];


    protected $hidden = ['media'];
   
    protected $appends = ['event_picture'];
    public function event()
    {
        return $this->belongsTo(FamilyEvent::class,'event_id','id');
    }

    public function childUser()
    {
        return $this->belongsTo(ChildUser::class,'child_id','id');
    }

    public function parentUser()
    {
        return $this->belongsTo(ChildUser::class,'parent_id','id');
    }



   public function getEventPictureAttribute()
    {
        if ($this->hasMedia('event_picture')){
            try {
                return $this->getFirstMedia('event_picture')->getTemporaryUrl(Carbon::now()->addMinutes(30),'thumb');
            } catch (\Throwable $th) {
                return $this->getFirstMedia('event_picture')->getUrl('thumb');
            }
        }
        return 'https://picsum.photos/200';
    }
}
