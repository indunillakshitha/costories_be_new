@extends('admin.app')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Donations</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
                <a href="{{ route('donation.index') }}" class="btn btn-info d-none d-lg-block m-l-15"><i
                        class="fa fa-plus-circle"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Donation </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="swalDefaultSuccess">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <form role="form" action="{{ route('donation.update', $donation->id) }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$donation->name}}" placeholder="Enter Name">
                        </div>
                        <div class="form-group">
                            <label for="contact">Contact</label>
                            <input type="number" class="form-control" id="contact" name="contact"value="{{$donation->contact}}" placeholder="Enter Contact">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email"value="{{$donation->email}}" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="account_type">Account Type</label>
                            <input type="text" class="form-control" id="account_type"value="{{$donation->account_type}}" name="account_type" placeholder="Enter Account Type">
                        </div>
                        <div class="form-group">
                            <label for="account_number">Account Number</label>
                            <input type="number" class="form-control" id="account_number"value="{{$donation->account_number}}" name="account_number" placeholder="Enter Account Number">
                        </div>
               


                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {{-- <button type="button" class="btn btn-primary" onclick="alerts()">Submit</button> --}}
                </form>
            </div>
            <!-- /.card -->
        </div>

    </div><!-- /.container-fluid -->

    </div>
    <!-- /.row -->



@endsection
